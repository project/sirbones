Sir Bones is a basic starter kit, allowing you to adapt the theme as you wish.

__________________________________________________________________________________________

Installation

- Sir Bones is meant to be YOUR theme. To change the name of the theme from 'sirbones' to another name like 'mytheme',
follow these steps (to do BEFORE enabling the theme) :

    - rename the theme folder to 'mytheme'
    - rename sirbones.info to mytheme.info
    - In template.php change each iteration of 'sirbones' to 'mytheme'

__________________________________________________________________________________________

Thanks for using SirBones, and remember to use the issue queue in drupal.org for any question
or bug report:

https://drupal.org/sandbox/dubcanada/1925000

Current maintainers:
* Steve Verbeek (dubcanada) -https://drupal.org/user/1422684 (http://modernmedia.ca)